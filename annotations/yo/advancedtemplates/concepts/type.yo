Type requirements specify types that must have been defined when using an
actual argument type. This normally refers to subtypes of the concept's
parameter type. In this case no tt(Type) parameter is required, and so the
tt(requires) parameter list is empty. The constraint is directly applied to
the concept's type parameter:
        verb(
    template <typename Type> 
    concept bool ValueType()
    {
        return 
            requires()
            {
                typename Type::value_type;
            };
    }
        )
    The type requirement doesn't necessarily refer to a type that is defined
as, e.g., a nested class or using a tt(typedef) specification. If the
specified type is a nested tt(enum) (not necessarily a strongly typed enum)
then a type requirement can also be specified to require the enum's name.
