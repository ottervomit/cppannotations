Nested requirements allow us to use already defined requirement specifications
in other requirement specifications. Consider iterators:
    itemization(
    it() Input iterators are incrementable, and support dereferencing to const
        values;
    it() Output iterators are like Input iterators, but refer to non-const
        values;
    it() Forward iterators combine Input and Output iterators;
    it() Bidirectional iterators are like Forward iterators, but also support
        decrement operators;
    it() RandomAccess iterators are like Bidirectional iterators, but in
        addition support any stepsize.
    )

Assume requirements for Input iterators and Output iterators were defined in,
respectively, tt(concept bool InputIterator()) and tt(concept bool
OutputIterator()).

To define the tt(concept bool ForwardIterator()) no additional requirements
need to be specified. We can merely `inherit' the requirements from the
existing tt(InputIterator) and tt(OutputIterator) concepts:
        verb(
    template <typename Type> 
    concept bool ForwardIterator()
    {
        return 
            requires
            {
                requires InputIterator<Type>();
                requires OutputIterator<Type>();
            };
    }
        )

At the next step, the tt(BidirectionalIterator) inherits an additional concept
(cf. section ref(REQSIMPLE)):
        verb(
    template <typename Type> 
    concept bool BidirectionalIterator()
    {
        return 
            requires
            {
                requires ForwardIterator<Type>();
                requires IncAndDec<Type>();
            };
    }
        )


