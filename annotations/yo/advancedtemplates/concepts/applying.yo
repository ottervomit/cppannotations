Once a concept has been defined the concept's function (or concept variable)
name can be used instead of tt(typename) in the definition of a template
header.

Assume the following concept has been defined:
        verb(
    template <typename Type>
    concept bool Addition =
        requires(Type lhs, Type rhs)
        {
            lhs + rhs;
        };
    )

To apply this concept to a template, tt(Addition) can be used instead of
tt(typename):
        verb(
    template <Addition Type>
    Type adder(Type const &lhs, Type const &rhs)
    {
        return lhs + rhs;
    }
        )

Alternatively,
    itemization(
    it() tt(typename) can still be used, but the concept can explicitly be
required:
    verb(
template <typename Type>   
    requires Addition<Type>         // explicitly requiring the concept
Type adder(Type const &lhs, Type const &rhs)
...
    )

    it() Or an explicit emi(requires clause) can be specified:
    verb(
template <typename Type>
    requires requires(Type lhs, Type rhs)   // explicitly specify the
            {                               // requirement
                lhs + rhs;
            }
Type adder(Type const &lhs, Type const &rhs)
...
    )
    In this example the use of two tt(requires) keywords should be noted: the
first one introduces the requires-clause, the second one defines the
requires-expression. The requires-expression may consist of multiple
requires-expressions, combined using tt(and) and/or tt(or) operators.
    )

There is no formal reason for preferring one form over the others. As a 
 i(rule of thumb) the third form might be used if the requirement is simple
and not repeatedly used, whereas the first might be preferred for more complex
and/or repeatedly used requirements. The latter would (eventually) result in a
emi(concept library).  

Example: if tt(adder) is used in a program, passing tt(int) values as its
arguments the requirements are satisfied, and compilation succeeds. But
if, e.g., tt(vectors) are used as arguments the compiler reports:
    verb(
error: cannot call function ‘Type adder(Type, Type) 
                                        [with Type = std::vector<int>]’
     adder(vi, vi);
                 ^
note: constraints not satisfied
note: with `std::vector<int> lhs'
note: with `std::vector<int> rhs'
note: the required expression `(lhs + rhs)' would be ill-formed
    )


